package com.example.security_backend;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingsController {

    @GetMapping("sayHello")
    public String sayHello() {
        return "Hello World";
    }
}
